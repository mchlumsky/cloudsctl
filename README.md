[![pipeline status](https://gitlab.com/mchlumsky/cloudsctl/badges/master/pipeline.svg)](https://gitlab.com/mchlumsky/cloudsctl/-/commits/master)
[![documentation](https://docs.rs/cloudsctl/badge.svg)](https://docs.rs/cloudsctl)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Latest Version](https://img.shields.io/crates/v/cloudsctl.svg)](https://crates.io/crates/cloudsctl)

`cloudsctl` (pronounced "cloud skittle") allows the user to modify their
OpenStack configuration (clouds.yaml, secure.yaml and clouds-public.yaml files)
with a CLI. Only files under $HOME_DIR/.config/openstack/ are read from or
written to.

`cloudsctl` can also inject cloud variables from clouds.yaml, secure.yaml and
clouds-public.yaml into the environment and run a command. This allows you to
keep all your cloud configuration in one place and get rid of any Openstack RC
files you might have lying around.

See [here](https://mchlumsky.gitlab.io/cloudsctl/cloudsctl/) for more details.

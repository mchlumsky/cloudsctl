//! # cloudsctl
//!
//! `cloudsctl` (pronounced "cloud skittle") allows the user to modify their OpenStack
//! configuration (clouds.yaml, secure.yaml and clouds-public.yaml files) with a CLI.
//!
//! `cloudsctl` can also inject cloud variables from clouds.yaml, secure.yaml and
//! clouds-public.yaml into the environment and run a command. This allows you to keep all your
//! cloud configuration in one place and get rid of any Openstack RC files you might have lying
//! around.
//!
//! ## Quickstart
//!
//! ### Create a profile
//!
//! ```shell
//! $ cloudsctl profile create --auth-url https://my-cloud.domain.tld:5000/v3 my-profile
//! ```
//!
//! This will create an OpenStack profile called `my-profile` in your `clouds-public.yaml` file.
//!
//! ### Show a profile
//!
//! ```shell
//! $ cloudsctl profile show my-profile
//! ```
//!
//! This will show you the profile `my-profile`
//!
//! ### List profiles
//!
//! ```shell
//! $ cloudsctl profile list
//! ```
//!
//! This will list all profiles in `clouds-public.yaml`
//!
//! ### Delete a profile
//!
//! ```shell
//! $ cloudsctl profile delete my-profile
//! ```
//!
//! This will delete the profile called `my-profile` from `clouds-public.yaml`
//!
//! ### Set profile settings
//!
//! ```shell
//! $ cloudsctl profile set --auth-url https://new-api:5000/v3 my-profile
//! ```
//!
//! This will change the auth-url for the profile `my-profile` to `https://new-api:5000/v3` in
//! clouds-public.yaml.
//!
//! Run `cloudsctl profile set --help` to see all the settings that you can change for a profile.
//!
//! ### Create a cloud
//!
//! ```shell
//! $ cloudsctl cloud create --username my-user --password-prompt --profile my-profile my-cloud
//! ```
//!
//! This will:
//! - create a cloud named `my-cloud` in `clouds.yaml` that uses the `my-profile` profile from `clouds-public.yaml`
//! - put your password in `secure.yaml`
//!
//! ### Show a cloud
//!
//! ```shell
//! $ cloudsctl cloud show my-cloud
//! ```
//!
//! This will show you the cloud `my-cloud`.
//!
//! ### List clouds
//!
//! ```shell
//! $ cloudsctl cloud list
//! ```
//!
//! This will list all clouds in `clouds.yaml`
//!
//! ### Delete a cloud
//!
//! ```shell
//! $ cloudsctl cloud delete my-cloud
//! ```
//!
//! This will delete the cloud called `my-cloud` from `clouds.yaml`
//!
//! ### Set cloud settings
//!
//! ```shell
//! $ cloudsctl cloud set --auth-url https://new-api:5000/v3 my-cloud
//! ```
//!
//! This will change the auth-url for the cloud `my-cloud` to `https://new-api:5000/v3` in
//! clouds.yaml.
//!
//! Run `cloudsctl cloud set --help` to see all the settings that you can change for a cloud.
//!
//! ### Copy cloud
//!
//! ```shell
//! $ cloudsctl cloud copy --username new_user source_cloud target_cloud
//! ```
//!
//! This will copy cloud `source_cloud` to `target_cloud` and set the user to `new_user` in the
//! target_cloud.
//!
//! Run `cloudsctl cloud copy --help` to see all the settings that you can override in the target
//! cloud while copying from the source cloud.
//!
//! ### Inject cloud variables from configuration files into environment and run command.
//!
//! ```shell
//! $ cloudsctl cloud create --username my-user --password-prompt --project-name my-project --profile my-profile my-cloud
//! Password:
//! $ cloudsctl env my-cloud bash
//! $ env|grep OS_
//! OS_AUTH_URL=https://my-cloud.domain.tld:5000/v3
//! OS_VERIFY=false
//! OS_TENANT_NAME=my-project
//! OS_USERNAME=my-user
//! OS_PROJECT_NAME=my-project
//! OS_PASSWORD=foo
//! ```
//!
//! `bash` is the default command and could have been omitted in the command above.
//!
//! I like to keep an alias called `oscloud` like this:
//!
//! ```shell
//! $ alias oscloud='cloudsctl env'
//! $ oscloud my-cloud
//! $ openstack server list
//! etc...
//! ```
//!
//! ### Shell completion
//!
//! Thanks to [clap](https://docs.rs/clap/), cloudsctl supports shell completion for bash, zsh,
//! fish and elvish.
//!
//! ```shell
//! $ cloudsctl completion bash
//! ```
//!
//! A completion file will be generated in the current directory.
//!

#![deny(
    bare_trait_objects,
    const_err,
    dead_code,
    improper_ctypes,
    missing_copy_implementations,
    missing_debug_implementations,
//    missing_docs,
    non_shorthand_field_patterns,
    no_mangle_generic_items,
    overflowing_literals,
    path_statements,
    patterns_in_fns_without_body,
    private_in_public,
    trivial_casts,
    trivial_numeric_casts,
    unconditional_recursion,
    unsafe_code,
    unused,
    unused_allocation,
    unused_comparisons,
    unused_doc_comments,
    unused_import_braces,
    unused_parens,
    unused_qualifications,
    unused_results,
    while_true
)]

use thiserror::Error;

pub mod cli;
pub mod cloud_config;
pub mod format;

#[derive(Error, Debug)]
pub enum CloudsCtlError {
    #[error("Cloud {0} exists already.")]
    CloudAlreadyExists(String),

    #[error("Cloud {0} not found.")]
    CloudNotFound(String),

    #[error("Profile {0} exists already.")]
    ProfileAlreadyExists(String),

    #[error("Profile {0} not found.")]
    ProfileNotFound(String),

    #[error("Programming error: {0}.")]
    ProgrammingError(String),

    #[error("Config path not found for {0}.")]
    ConfigPathNotFound(String),

    #[error("Unknown output format: {0}")]
    UnknownOutputFormat(String),
}

//! Models for OpenStack configuration.
//!
//! This module contains all the models for the contents of OpenStack configuration files with some
//! implementation as well.

use crate::cli::CommonOpts;
use crate::CloudsCtlError;
use anyhow::{Context, Result};
use indexmap::IndexMap;
use path::PathBuf;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_with_macros::skip_serializing_none;
use std::{collections::HashMap, fs, path};

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize, Clone, Default, PartialEq)]
pub struct Auth {
    pub auth_url: Option<String>,
    pub password: Option<String>,
    pub project_domain_name: Option<String>,
    pub project_name: Option<String>,
    pub user_domain_name: Option<String>,
    pub username: Option<String>,
    pub domain_name: Option<String>,
}

impl From<Auth> for IndexMap<String, String> {
    fn from(auth: Auth) -> Self {
        let mut map = IndexMap::new();

        let _ = map.insert("auth_url".into(), auth.auth_url.unwrap_or_default());
        let _ = map.insert("password".into(), auth.password.unwrap_or_default());
        let _ = map.insert(
            "project_domain_name".into(),
            auth.project_domain_name.unwrap_or_default(),
        );
        let _ = map.insert("project_name".into(), auth.project_name.unwrap_or_default());
        let _ = map.insert(
            "user_domain_name".into(),
            auth.user_domain_name.unwrap_or_default(),
        );
        let _ = map.insert("username".into(), auth.username.unwrap_or_default());
        let _ = map.insert("domain_name".into(), auth.domain_name.unwrap_or_default());

        map
    }
}

impl Auth {
    fn override_with(&mut self, other: &Auth) {
        self.username = other.username.clone().or_else(|| self.username.take());

        self.password = other.password.clone().or_else(|| self.password.take());

        self.project_name = other
            .project_name
            .clone()
            .or_else(|| self.project_name.take());

        self.project_domain_name = other
            .project_domain_name
            .clone()
            .or_else(|| self.project_domain_name.take());

        self.user_domain_name = other
            .user_domain_name
            .clone()
            .or_else(|| self.user_domain_name.take());

        self.domain_name = other
            .domain_name
            .clone()
            .or_else(|| self.domain_name.take());

        self.auth_url = other.auth_url.clone().or_else(|| self.auth_url.take());
    }
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize, Clone, Default, PartialEq)]
pub struct Cloud {
    pub auth: Option<Auth>,
    pub region_name: Option<String>,
    pub identity_api_version: Option<String>,
    pub volume_api_version: Option<String>,
    pub identity_endpoint_override: Option<String>,
    pub verify: Option<bool>,
    pub cacert: Option<String>,
    pub profile: Option<String>,
}

impl From<Cloud> for IndexMap<String, String> {
    fn from(cloud: Cloud) -> Self {
        let mut map = IndexMap::new();
        let auth: IndexMap<String, String> = cloud.auth.unwrap_or_default().into();
        map.extend(auth);

        let _ = map.insert("region_name".into(), cloud.region_name.unwrap_or_default());
        let _ = map.insert(
            "identity_api_version".into(),
            cloud.identity_api_version.unwrap_or_default(),
        );
        let _ = map.insert(
            "volume_api_version".into(),
            cloud.volume_api_version.unwrap_or_default(),
        );
        let _ = map.insert(
            "identity_endpoint_override".into(),
            cloud.identity_endpoint_override.unwrap_or_default(),
        );
        let _ = map.insert(
            "verify".into(),
            cloud.verify.map_or(String::new(), |v| {
                if v {
                    "true".into()
                } else {
                    "false".into()
                }
            }),
        );
        let _ = map.insert("cacert".into(), cloud.cacert.unwrap_or_default());
        let _ = map.insert("profile".into(), cloud.profile.unwrap_or_default());

        map
    }
}

impl From<CommonOpts> for Cloud {
    fn from(common_opts: CommonOpts) -> Self {
        Cloud {
            auth: Some(Auth {
                username: common_opts.username,
                password: common_opts.password,
                project_name: common_opts.project_name,
                project_domain_name: common_opts.project_domain_name,
                user_domain_name: common_opts.user_domain_name,
                domain_name: common_opts.domain_name,
                auth_url: common_opts.auth_url,
            }),
            region_name: common_opts.region_name,
            volume_api_version: common_opts.volume_api_version,
            identity_api_version: common_opts.identity_api_version,
            identity_endpoint_override: common_opts.identity_endpoint_override,
            verify: common_opts.verify,
            cacert: common_opts.cacert,
            profile: None,
        }
    }
}

impl Cloud {
    pub fn override_with(&mut self, other: &Cloud) {
        self.region_name = other
            .region_name
            .clone()
            .or_else(|| self.region_name.take());

        self.identity_api_version = other
            .identity_api_version
            .clone()
            .or_else(|| self.identity_api_version.take());

        self.volume_api_version = other
            .volume_api_version
            .clone()
            .or_else(|| self.volume_api_version.take());

        self.identity_endpoint_override = other
            .identity_endpoint_override
            .clone()
            .or_else(|| self.identity_endpoint_override.take());

        self.verify = other.verify.clone().or_else(|| self.verify.take());

        self.cacert = other.cacert.clone().or_else(|| self.cacert.take());

        self.profile = other.profile.clone().or_else(|| self.profile.take());

        match self.auth {
            None => {
                self.auth = other.auth.clone();
            }
            Some(ref mut auth) => {
                if let Some(other_auth) = other.auth.as_ref() {
                    auth.override_with(other_auth);
                }
            }
        }
    }

    pub fn build_env(&self) -> Result<HashMap<String, String>> {
        let auth = self.auth.as_ref().context("Missing auth section.")?;

        let mut env_map: HashMap<String, String> = std::env::vars().collect();

        let _ = env_map.insert(
            "OS_USERNAME".into(),
            auth.username.as_ref().context("Missing username.")?.into(),
        );

        let _ = env_map.insert(
            "OS_PASSWORD".into(),
            auth.password.as_ref().context("Missing password.")?.into(),
        );

        let _ = env_map.insert(
            "OS_PROJECT_NAME".into(),
            auth.project_name
                .as_ref()
                .context("Missing project name.")?
                .into(),
        );

        let _ = env_map.insert(
            "OS_TENANT_NAME".into(),
            auth.project_name
                .as_ref()
                .context("Missing project name.")?
                .into(),
        );

        if let Some(project_domain_name) = &auth.project_domain_name {
            let _ = env_map.insert("OS_PROJECT_DOMAIN_NAME".into(), project_domain_name.into());
        }

        if let Some(user_domain_name) = &auth.user_domain_name {
            let _ = env_map.insert("OS_USER_DOMAIN_NAME".into(), user_domain_name.into());
        }

        if let Some(domain_name) = &auth.domain_name {
            let _ = env_map.insert("OS_DOMAIN_NAME".into(), domain_name.into());
        }

        let _ = env_map.insert(
            "OS_AUTH_URL".into(),
            auth.auth_url
                .as_ref()
                .context("Missing authentication url.")?
                .into(),
        );

        if let Some(region_name) = &self.region_name {
            let _ = env_map.insert("OS_REGION_NAME".into(), region_name.into());
        }

        if let Some(identity_api_version) = &self.identity_api_version {
            let _ = env_map.insert(
                "OS_IDENTITY_API_VERSION".into(),
                identity_api_version.into(),
            );
        }

        if let Some(volume_api_version) = &self.volume_api_version {
            let _ = env_map.insert("OS_VOLUME_API_VERSION".into(), volume_api_version.into());
        }

        if let Some(identity_endpoint_override) = &self.identity_endpoint_override {
            let _ = env_map.insert(
                "OS_IDENTITY_ENDPOINT_OVERRIDE".into(),
                identity_endpoint_override.into(),
            );
        }

        if let Some(verify) = &self.verify {
            let _ = env_map.insert("OS_VERIFY".into(), verify.to_string());
        }

        if let Some(cacert) = &self.cacert {
            let _ = env_map.insert("OS_CACERT".into(), cacert.into());
        }

        Ok(env_map)
    }
}

pub trait CloudGroup {
    fn get_clouds(&self) -> &HashMap<String, Cloud>;

    fn names(&self) -> Vec<&str> {
        self.get_clouds().keys().map(|key| key.as_str()).collect()
    }
}

impl CloudGroup for Clouds {
    fn get_clouds(&self) -> &HashMap<String, Cloud> {
        &self.clouds
    }
}

impl CloudGroup for PublicClouds {
    fn get_clouds(&self) -> &HashMap<String, Cloud> {
        &self.clouds
    }
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Clouds {
    pub clouds: HashMap<String, Cloud>,
}

impl Clouds {
    pub fn load_from_files(ignore_missing_profile: &bool) -> Result<Clouds> {
        let cloud_config_path = get_config_path("clouds.yaml")?;
        let public_cloud_config_path = get_config_path("clouds-public.yaml")?;
        let secure_config_path = get_config_path("secure.yaml")?;

        Clouds::from_files(
            cloud_config_path,
            public_cloud_config_path,
            secure_config_path,
            ignore_missing_profile,
        )
    }

    fn from_files(
        cloud_config_path: Option<PathBuf>,
        public_cloud_config_path: Option<PathBuf>,
        secure_config_path: Option<PathBuf>,
        ignore_missing_profile: &bool,
    ) -> Result<Clouds> {
        let cloud_config_path = cloud_config_path.context("clouds.yaml not found.")?;

        let mut clouds: Clouds = serde_yaml::from_str(
            &fs::read_to_string(cloud_config_path).context("Failed to read clouds.yaml")?,
        )
        .context("Failed to parse clouds.yaml")?;

        if let Some(path) = public_cloud_config_path {
            let mut public_clouds: PublicClouds = serde_yaml::from_str(&fs::read_to_string(path)?)
                .context("Failed to parse clouds-public.yaml")?;

            clouds
                .expand_profiles(&mut public_clouds)
                .or_else(|err_message| {
                    if !ignore_missing_profile {
                        Err(err_message)
                    } else {
                        Ok(())
                    }
                })?;
        }

        if let Some(path) = secure_config_path {
            clouds.override_with(
                &serde_yaml::from_str(&fs::read_to_string(path)?)
                    .context("Failed to parse secure.yaml")?,
            );
        }

        Ok(clouds)
    }

    pub fn as_files(&mut self, clouds_file: &path::Path, secure_file: &path::Path) -> Result<()> {
        let mut secure = Clouds {
            clouds: HashMap::new(),
        };

        for (name, cloud) in self.clouds.iter_mut() {
            let password = cloud
                .auth
                .as_ref()
                .context("Missing auth section.")?
                .password
                .clone();

            if password.is_some() {
                // Move password into secure.yaml
                let _ = secure.clouds.insert(
                    name.to_string(),
                    Cloud {
                        auth: Some(Auth {
                            password,
                            ..Default::default()
                        }),
                        ..Default::default()
                    },
                );

                // Remove password from clouds.yaml
                let auth = cloud.auth.as_mut().context("Missing auth section.")?;
                auth.password = None;
            }
        }
        fs::write(
            clouds_file,
            serde_yaml::to_string(self).context("Failed to generate clouds.yaml")?,
        )?;
        fs::write(
            secure_file,
            serde_yaml::to_string(&secure).context("Failed to generate secure.yaml")?,
        )?;
        Ok(())
    }

    fn expand_profiles(&mut self, public_clouds: &mut PublicClouds) -> Result<()> {
        for cloud in self.clouds.values_mut() {
            if let Some(profile) = &cloud.profile {
                let public_cloud = public_clouds
                    .clouds
                    .get_mut(profile)
                    .context(format!("profile {} not found", profile))?;
                public_cloud.override_with(&cloud);

                // Clone profile because profile comes from cloud which is borrowed and we
                // re-assign cloud completely next.
                let profile = profile.clone();

                *cloud = public_cloud.clone();
                cloud.profile = Some(profile);
            }
        }
        Ok(())
    }

    fn override_with(&mut self, other: &Clouds) {
        for (cloud_name, cloud) in other.clouds.iter() {
            match self.clouds.get_mut(cloud_name) {
                None => {
                    let _ = self.clouds.insert(cloud_name.to_string(), cloud.clone());
                }
                Some(self_cloud) => {
                    self_cloud.override_with(&cloud);
                }
            }
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PublicClouds {
    #[serde(rename = "public-clouds")]
    pub clouds: HashMap<String, Cloud>,
}

impl PublicClouds {
    pub fn to_file(&self, filepath: &path::Path) -> Result<()> {
        fs::write(
            filepath,
            serde_yaml::to_string(self).context("Failed to parse clouds-public.yaml.")?,
        )
        .context("Failed to write clouds-public.yaml.")
    }

    pub fn from_files() -> Result<Self>
    where
        Self: Sized + DeserializeOwned,
    {
        let filename = "clouds-public.yaml";
        if let Some(path) = get_config_path(filename)? {
            let clouds: Self = serde_yaml::from_str(
                &fs::read_to_string(path).context(format!("Failed to read {}", filename))?,
            )
            .context(format!("Failed to parse {}", filename))?;
            Ok(clouds)
        } else {
            Err(CloudsCtlError::ConfigPathNotFound(filename.into()).into())
        }
    }
}

fn get_home_config_dir() -> Result<PathBuf> {
    Ok(dirs::home_dir()
        .context("Failed to obtain home directory")?
        .join(".config/openstack"))
}

fn get_config_path(filename: &str) -> Result<Option<PathBuf>> {
    let path = get_home_config_dir()?.join(filename);
    if path.exists() {
        Ok(Some(path))
    } else {
        Ok(None)
    }
}

pub fn create_and_get_path(filename: &str) -> Result<PathBuf> {
    let config_dir = get_home_config_dir()?;
    fs::create_dir_all(config_dir.as_path()).unwrap_or(());
    Ok(config_dir.join(filename))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::cli::{CloudCommand, Command, Opt};
    use crate::format::Format;
    use std::io::Write;
    use tempfile::NamedTempFile;

    fn generate_auth(index: isize) -> Auth {
        Auth {
            username: Some(format!("{}{}", "user", index)),
            password: Some(format!("{}{}", "password", index)),
            project_name: Some(format!("{}{}", "project", index)),
            project_domain_name: Some(format!("{}{}", "domain", index)),
            user_domain_name: Some(format!("{}{}", "domain", index)),
            domain_name: Some(format!("{}{}", "domain", index)),
            auth_url: Some(format!("{}{}", "url", index)),
        }
    }

    fn generate_cloud(index: isize) -> Cloud {
        Cloud {
            auth: Some(generate_auth(index)),
            region_name: Some(format!("{}{}", "region", index)),
            identity_api_version: Some(format!("{}{}", "", index)),
            volume_api_version: Some(format!("{}{}", "", index)),
            identity_endpoint_override: Some(format!("{}{}", "url", index)),
            verify: Some(index % 2 == 0),
            cacert: Some(format!("{}{}", "cert", index)),
            profile: Some(format!("{}{}", "profile", index)),
        }
    }

    fn generate_clouds(index: isize) -> Clouds {
        let cloud = generate_cloud(index);
        let mut clouds = HashMap::new();
        let _ = clouds.insert("cloud_name".into(), cloud);
        Clouds { clouds }
    }

    #[test]
    fn auth_override() {
        let mut auth1 = generate_auth(1);
        let auth2 = generate_auth(2);

        auth1.override_with(&auth2);

        assert_eq!(auth1, auth2);
    }

    #[test]
    fn cloud_override() {
        let mut cloud1 = generate_cloud(1);
        let cloud2 = generate_cloud(2);

        cloud1.override_with(&cloud2);

        assert_eq!(cloud1, cloud2);
    }

    #[test]
    fn cloud_build_env() {
        let cloud = generate_cloud(1);

        let mut expected: HashMap<String, String> = std::env::vars().collect();
        let _ = expected.insert("OS_USERNAME".into(), "user1".into());
        let _ = expected.insert("OS_PASSWORD".into(), "password1".into());
        let _ = expected.insert("OS_PROJECT_NAME".into(), "project1".into());
        let _ = expected.insert("OS_TENANT_NAME".into(), "project1".into());
        let _ = expected.insert("OS_PROJECT_DOMAIN_NAME".into(), "domain1".into());
        let _ = expected.insert("OS_USER_DOMAIN_NAME".into(), "domain1".into());
        let _ = expected.insert("OS_DOMAIN_NAME".into(), "domain1".into());
        let _ = expected.insert("OS_AUTH_URL".into(), "url1".into());
        let _ = expected.insert("OS_REGION_NAME".into(), "region1".into());
        let _ = expected.insert("OS_IDENTITY_API_VERSION".into(), "1".into());
        let _ = expected.insert("OS_VOLUME_API_VERSION".into(), "1".into());
        let _ = expected.insert("OS_IDENTITY_ENDPOINT_OVERRIDE".into(), "url1".into());
        let _ = expected.insert("OS_VERIFY".into(), "false".into());
        let _ = expected.insert("OS_CACERT".into(), "cert1".into());

        assert_eq!(expected, cloud.build_env().unwrap());
    }

    #[test]
    #[should_panic(expected = "called `Result::unwrap()` on an `Err` value: Missing auth section.")]
    fn cloud_build_env_panic() {
        let cloud = Cloud {
            ..Default::default()
        };

        let _ = cloud.build_env().unwrap();
    }

    #[test]
    fn cloud_from_commonopts() {
        let args = vec![
            "myprog",
            "cloud",
            "create",
            "cloud_name",
            "--username",
            "user1",
            "--password",
            "pass1",
            "--project-name",
            "project1",
            "--project-domain-name",
            "domain1",
            "--user-domain-name",
            "domain1",
            "--domain-name",
            "domain1",
            "--auth-url",
            "http://url1",
            "--region-name",
            "region1",
            "--identity-api-version",
            "3",
            "--volume-api-version",
            "3",
            "--identity-endpoint-override",
            "http://url2",
            "--cacert",
            "cert1.pem",
            "--profile",
            "profile1",
            "--verify",
            "true",
        ];

        let opt: Opt = structopt::StructOpt::from_iter(args);

        let expected = Cloud {
            auth: Some(Auth {
                username: Some("user1".into()),
                password: Some("pass1".into()),
                project_name: Some("project1".into()),
                project_domain_name: Some("domain1".into()),
                user_domain_name: Some("domain1".into()),
                domain_name: Some("domain1".into()),
                auth_url: Some("http://url1".into()),
            }),
            verify: Some(true),
            region_name: Some("region1".into()),
            identity_api_version: Some("3".into()),
            volume_api_version: Some("3".into()),
            identity_endpoint_override: Some("http://url2".into()),
            cacert: Some("cert1.pem".into()),
            profile: None,
        };

        match opt {
            Opt {
                format,
                cmd:
                    Command::Cloud(CloudCommand::Create {
                        cloud,
                        common_opts,
                        profile,
                    }),
            } => {
                assert_eq!(Cloud::from(common_opts), expected);
                assert_eq!(profile, Some("profile1".into()));
                assert_eq!(cloud, "cloud_name");
                assert_eq!(format, Format::table);
            }
            opt => {
                panic!("Unexpected Opt: {:#?}", opt);
            }
        }
    }

    /// Test Clouds::from_files when there are no files
    #[test]
    #[should_panic(expected = "clouds.yaml not found.")]
    fn clouds_from_files_no_files_exist() {
        let clouds = Clouds::from_files(None, None, None, &false).unwrap();

        let expected = generate_clouds(1);
        assert_eq!(expected, clouds);
    }

    /// Test Clouds::from_files when there is only clouds.yaml
    #[test]
    fn clouds_from_files_clouds_only() {
        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile1
    verify: false"#;
        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();

        let cloud_config_path = Some(cloud_config.path().to_path_buf());
        let clouds = Clouds::from_files(cloud_config_path, None, None, &false).unwrap();

        let expected = generate_clouds(1);
        assert_eq!(expected, clouds);
    }

    /// Test Clouds::from_files when there is clouds.yaml and clouds-public.yaml
    #[test]
    fn clouds_from_files_clouds_public() {
        let empty_clouds_data = r#"
clouds:
  cloud_name:
    auth: {}
    profile: profile1"#;

        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile1
    verify: false"#;

        let public_clouds_data = r#"
public-clouds:
  profile1:
    auth:
      username: user2
      password: password2
      project_name: project2
      user_domain_name: domain2
      project_domain_name: domain2
      domain_name: domain2
      auth_url: "url2"
    region_name: region2
    identity_api_version: "2"
    volume_api_version: "2"
    identity_endpoint_override: url2
    cacert: cert2
    verify: true"#;

        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();
        let cloud_config_path = Some(cloud_config.path().to_path_buf());

        let mut public_cloud_config = NamedTempFile::new().unwrap();
        write!(public_cloud_config, "{}", public_clouds_data).unwrap();
        let public_cloud_config_path = Some(public_cloud_config.path().to_path_buf());

        let clouds = Clouds::from_files(
            cloud_config_path,
            public_cloud_config_path.clone(),
            None,
            &false,
        )
        .unwrap();

        let expected = generate_clouds(1);

        // clouds.yaml completely overrides public-clouds.yaml
        assert_eq!(expected, clouds);

        let mut empty_cloud_config = NamedTempFile::new().unwrap();
        write!(empty_cloud_config, "{}", empty_clouds_data).unwrap();
        let empty_cloud_config_path = Some(empty_cloud_config.path().to_path_buf());

        let clouds = Clouds::from_files(
            empty_cloud_config_path,
            public_cloud_config_path,
            None,
            &false,
        )
        .unwrap();

        let mut expected = generate_clouds(2);
        expected.clouds.get_mut("cloud_name").unwrap().profile = Some("profile1".into());

        // clouds.yaml is empty so public-clouds.yaml takes precendence
        assert_eq!(expected, clouds);
    }

    /// Test Clouds::from_files when there is clouds.yaml and clouds-public.yaml and secure.yaml
    #[test]
    fn clouds_from_files_clouds_public_secure() {
        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile1
    verify: false"#;

        let public_clouds_data = r#"
public-clouds:
  profile1:
    auth:
      username: user2
      password: password2
      project_name: project2
      user_domain_name: domain2
      project_domain_name: domain2
      domain_name: domain2
      auth_url: "url2"
    region_name: region2
    identity_api_version: "2"
    volume_api_version: "2"
    identity_endpoint_override: url2
    cacert: cert2
    verify: true"#;

        let secure_clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user3
      password: password3
      project_name: project3
      user_domain_name: domain3
      project_domain_name: domain3
      domain_name: domain3
      auth_url: "url3"
    region_name: region3
    identity_api_version: "3"
    volume_api_version: "3"
    identity_endpoint_override: url3
    cacert: cert3
    profile: profile3
    verify: false"#;

        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();
        let cloud_config_path = Some(cloud_config.path().to_path_buf());

        let mut public_cloud_config = NamedTempFile::new().unwrap();
        write!(public_cloud_config, "{}", public_clouds_data).unwrap();
        let public_cloud_config_path = Some(public_cloud_config.path().to_path_buf());

        let mut secure_cloud_config = NamedTempFile::new().unwrap();
        write!(secure_cloud_config, "{}", secure_clouds_data).unwrap();
        let secure_cloud_config_path = Some(secure_cloud_config.path().to_path_buf());

        let clouds = Clouds::from_files(
            cloud_config_path,
            public_cloud_config_path,
            secure_cloud_config_path,
            &false,
        )
        .unwrap();

        let mut expected_clouds = HashMap::new();
        let _ = expected_clouds.insert("cloud_name".into(), generate_cloud(3));
        let expected = Clouds {
            clouds: expected_clouds,
        };

        // secure.yaml overrides everything else
        assert_eq!(expected, clouds);
    }

    #[test]
    fn clouds_from_files_ignore_missing_profile_false_with_panic() {
        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile2
    verify: false"#;

        let public_clouds_data = r#"
public-clouds:
  profile1:
    auth:
      username: user2
      password: password2
      project_name: project2
      user_domain_name: domain2
      project_domain_name: domain2
      domain_name: domain2
      auth_url: "url2"
    region_name: region2
    identity_api_version: "2"
    volume_api_version: "2"
    identity_endpoint_override: url2
    cacert: cert2
    verify: true"#;

        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();
        let cloud_config_path = Some(cloud_config.path().to_path_buf());

        let mut public_cloud_config = NamedTempFile::new().unwrap();
        write!(public_cloud_config, "{}", public_clouds_data).unwrap();
        let public_cloud_config_path = Some(public_cloud_config.path().to_path_buf());

        let error = Clouds::from_files(cloud_config_path, public_cloud_config_path, None, &false);
        assert!(error.is_err());
        assert_eq!("profile profile2 not found", error.unwrap_err().to_string());
    }

    #[test]
    fn clouds_from_files_ignore_missing_profile_false_no_panic() {
        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile1
    verify: false"#;

        let public_clouds_data = r#"
public-clouds:
  profile1:
    auth:
      username: user2
      password: password2
      project_name: project2
      user_domain_name: domain2
      project_domain_name: domain2
      domain_name: domain2
      auth_url: "url2"
    region_name: region2
    identity_api_version: "2"
    volume_api_version: "2"
    identity_endpoint_override: url2
    cacert: cert2
    verify: true"#;

        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();
        let cloud_config_path = Some(cloud_config.path().to_path_buf());

        let mut public_cloud_config = NamedTempFile::new().unwrap();
        write!(public_cloud_config, "{}", public_clouds_data).unwrap();
        let public_cloud_config_path = Some(public_cloud_config.path().to_path_buf());

        let _ = Clouds::from_files(cloud_config_path, public_cloud_config_path, None, &false);
    }

    #[test]
    fn clouds_from_files_ignore_missing_profile_true() {
        let clouds_data = r#"
clouds:
  cloud_name:
    auth:
      username: user1
      password: password1
      project_name: project1
      user_domain_name: domain1
      project_domain_name: domain1
      domain_name: domain1
      auth_url: "url1"
    region_name: region1
    identity_api_version: "1"
    volume_api_version: "1"
    identity_endpoint_override: url1
    cacert: cert1
    profile: profile2
    verify: false"#;

        let public_clouds_data = r#"
public-clouds:
  profile1:
    auth:
      username: user2
      password: password2
      project_name: project2
      user_domain_name: domain2
      project_domain_name: domain2
      domain_name: domain2
      auth_url: "url2"
    region_name: region2
    identity_api_version: "2"
    volume_api_version: "2"
    identity_endpoint_override: url2
    cacert: cert2
    verify: true"#;

        let mut cloud_config = NamedTempFile::new().unwrap();
        write!(cloud_config, "{}", clouds_data).unwrap();
        let cloud_config_path = Some(cloud_config.path().to_path_buf());

        let mut public_cloud_config = NamedTempFile::new().unwrap();
        write!(public_cloud_config, "{}", public_clouds_data).unwrap();
        let public_cloud_config_path = Some(public_cloud_config.path().to_path_buf());

        let _ = Clouds::from_files(cloud_config_path, public_cloud_config_path, None, &true);
    }

    #[test]
    fn clouds_names() {
        let clouds = generate_clouds(1);
        assert_eq!(vec![String::from("cloud_name")], clouds.names());
    }

    #[test]
    fn clouds_names_empty() {
        let clouds = Clouds {
            clouds: HashMap::new(),
        };
        let expected: Vec<String> = vec![];
        assert_eq!(expected, clouds.names());
    }

    #[test]
    fn auth_from() {
        let auth = generate_auth(1);

        let mut expected = IndexMap::new();
        let _ = expected.insert(String::from("auth_url"), String::from("url1"));
        let _ = expected.insert(String::from("password"), String::from("password1"));
        let _ = expected.insert(String::from("project_domain_name"), String::from("domain1"));
        let _ = expected.insert(String::from("project_name"), String::from("project1"));
        let _ = expected.insert(String::from("user_domain_name"), String::from("domain1"));
        let _ = expected.insert(String::from("username"), String::from("user1"));
        let _ = expected.insert(String::from("domain_name"), String::from("domain1"));

        assert_eq!(expected, auth.into());
    }

    #[test]
    fn cloud_from() {
        let cloud = generate_cloud(1);

        let mut expected = IndexMap::new();
        let _ = expected.insert(String::from("auth_url"), String::from("url1"));
        let _ = expected.insert(String::from("password"), String::from("password1"));
        let _ = expected.insert(String::from("project_domain_name"), String::from("domain1"));
        let _ = expected.insert(String::from("project_name"), String::from("project1"));
        let _ = expected.insert(String::from("user_domain_name"), String::from("domain1"));
        let _ = expected.insert(String::from("username"), String::from("user1"));
        let _ = expected.insert(String::from("domain_name"), String::from("domain1"));
        let _ = expected.insert(String::from("region_name"), String::from("region1"));
        let _ = expected.insert(String::from("identity_api_version"), String::from("1"));
        let _ = expected.insert(String::from("volume_api_version"), String::from("1"));
        let _ = expected.insert(
            String::from("identity_endpoint_override"),
            String::from("url1"),
        );
        let _ = expected.insert(String::from("verify"), String::from("false"));
        let _ = expected.insert(String::from("cacert"), String::from("cert1"));
        let _ = expected.insert(String::from("profile"), String::from("profile1"));

        assert_eq!(expected, cloud.into());
    }
}

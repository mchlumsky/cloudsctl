use anyhow::Result;
use cloudsctl::cli;
use std::process;

fn main() {
    if let Result::Err(e) = cli::run() {
        eprintln!("Error: {:?}", e);
        process::exit(1);
    };
}
